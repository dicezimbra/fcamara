# Projeto mobile FCamara
A autenticação é feita utilizando o firebase do Google.

Vantagens Firebase -> 
* Simples implementação
* Apis prontas para autenticação por email, facebook, google account, etc.
* Fácil integração com o banco de dados

No layout foi utilizado para a maioria, o material design que é estudado e melhorado todos dias pela Google.

Para gerar o APK:
Vá até a pasta APPMOBILE abra o prompt de comando e rode o seguinte comando:
gradlew.bat assembleRelease

Há um apk pronto para ser instalado na mesma.


# Projeto backend 
Para o backend escolhi desenvolver usando node.js e no servidor utilizei os serviços do OPENSHIFT

Vantagens Node.js -> 
* Aplicação escrita em javascript
* IO não-bloqueante, ou seja, nenhuma tarefa pesadas de IO vai travar sua aplicação, pois elas serão executadas em background
* Suporta milhares de requisições simultâneas

Vantagens OPENSHIFT -> Gratuito até determinada quantidade de requisições e armazenamento. 