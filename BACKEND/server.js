var app_name = "avenuecode";
/**
 * Module dependencies.
 */
var express = require('express');
var MongoStore = require('connect-mongo')(express);
var path = require('path');
var mongoose = require('mongoose');
var expressValidator = require('express-validator');
var http = require('http');


/**
 * Create Express server.
 */
var app = express();

var dbHost = process.env.OPENSHIFT_MONGODB_DB_URL || "mongodb://localhost:27017/";
var dbName = process.env.OPENSHIFT_APP_NAME       || app_name;
var dbUrl = "" + dbHost + dbName;

/**
 * Mongoose configuration.
 */

mongoose.connect(dbUrl);
mongoose.connection.on('error', function() {
	console.error('MongoDB Connection Error. Please make sure MongoDB is running.');
});

/**
 * Express configuration.
 */

var hour = 3600000;
var day = (hour * 24);
var week = (day * 7);
var month = (day * 30);

var ipaddr = process.env.OPENSHIFT_NODEJS_IP || "0.0.0.0";
var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 3000;

//all environments
app.set('port', port);
app.set('ipaddr', ipaddr);

//app.set('view engine', 'html');

app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.cookieParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, '/public'), {
    extensions: ['html', 'js', 'css', 'png', 'jpg'],
    maxAge: day
}));
app.use(express.session({
	secret: "secret of my backend app "+app_name,
	cookie: { maxAge: day },
	store: new MongoStore({
		db: mongoose.connection.db,
		clear_interval: 3600,
		auto_reconnect: true
	})
}));
app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));

//app.enable('trust proxy');

/**
 * Load controllers V1.
 */
var mschedule_controller_v1 = require('./controllers/mschedule/v1/mschedule_controller_v1');

/****************************************************************
 * 		HOME PAGE
 ***************************************************************/
//  get

var index = function (req, res) {
    res.send(200);
};
app.get(' ', index);
app.get('/', index);

/**************************************************************
 * 	MOBILE		 			MPost - V1                                  
 **************************************************************/  
//  get
app.post('/v1/addschedule', mschedule_controller_v1.addSchedule); 
app.post('/v1/editschedule', mschedule_controller_v1.editSchedule); 
app.get('/v1/getschedules', mschedule_controller_v1.getSchedules); 
app.get('/v1/deleteschedule', mschedule_controller_v1.deleteSchedule);  


/****************************************************************
 * 		INICIANDO SERVIDOR
 ***************************************************************/

/**
 * Start Express server.
 */
var server = http.createServer(app).listen(app.get('port'), app.get('ipaddr'), function(){
    
	console.log('%s: Express server started on %s:%d ...',
			Date(Date.now() ), app.get('ipaddr'), app.get('port') );
});
var io = require('socket.io')(server);

exports.io = io;
