var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
  id: { type: String},
  userid: { type: String},
  description: { type: String, default: '' },
  name: { type: String, default: '' },
  time: Number,

  antecedence: Number
});

module.exports = mongoose.model('MSchedule', userSchema);
