var path_root = "../../../";

var MSchedule = require(path_root+'models/MSchedule');
var fs = require('fs');
var path = require('path');

exports.addSchedule = function(req, res) {

	var resultFunction = function(cod,msg){
		var result = new function(){
			this.v = 1;
			this.cod = cod;
			this.msg = msg;	
		};
        
		// JSON
		res.end(JSON.stringify({
			ret: result
		}));
	};

	var p1_userid = req.body.userid;
	var p2_description = req.body.description;
	var p3_name = req.body.name;
	var p4_time = req.body.time;
	var p5_antecedence = req.body.antecedence;
	
	if(!p1_userid || !p3_name || !p4_time){
		resultFunction(1,"Error : Invalid params");
			return;
	}

	
	var obj = new MSchedule();
	obj.userid = p1_userid;
	obj.description = p2_description;
	obj.name = p3_name;
	obj.time = p4_time;
	obj.antecedence = p5_antecedence;
		
	obj.save(function(err) {
		if (err) {
			resultFunction(2,"Error :"+err);
			return;      
		} 
						
		resultFunction(0,""+obj._id);   
	});
};

exports.editSchedule = function(req, res) {

	var resultFunction = function(cod,msg){
		var result = new function(){
			this.v = 1;
			this.cod = cod;
			this.msg = msg;	
		};
        
		// JSON
		res.end(JSON.stringify({
			ret: result
		}));
	};

	var p1_userid = req.body.userid;
	var p2_description = req.body.description;
	var p3_name = req.body.name;
	var p4_time = req.body.time;
	var p5_antecedence = req.body.antecedence;
	var p6_schedule_id = req.body.id;
	console.log(p6_schedule_id);

	MSchedule.findOne({_id: p6_schedule_id}, function(err, obj) {	
				
		if(err){ 
			resultFunction(1,"Error :"+err);
			return;
		}

		if(!obj){
			resultFunction(2,"Does not exist");
			return;
		}


		obj.userid = p1_userid;
		obj.description = p2_description;
		obj.name = p3_name;
		obj.time = p4_time;
		obj.antecedence = p5_antecedence;
		
		obj.save(function(err) {
			if (err) {
				resultFunction(3,"Error :"+err);
				return;      
			} 			
			resultFunction(0,""+obj._id);   
		});
	});

}

exports.getSchedules = function(req, res) {
	var resultFunction = function(cod,msg){
		var result = new function(){
			this.v = 1;
			this.cod = cod;
			this.msg = msg;	
		};
        
		// JSON
		res.end(JSON.stringify({
			ret: result
		}));
	};

  
	var p1_user_id = req.query.p1;
	console.log("p1 = " + p1_user_id);
	var _buff = [];
	MSchedule.find({userid: p1_user_id}, function(err, obj) {	
				
		

		if(err){ 
			resultFunction(-1,"Error :"+err);
			return;
		}

		if(!obj){
			resultFunction(-2,"Don't have schedules");
			return;
		}
			           
		if(obj.length==0){
			resultFunction(-3,"Don't have schedules");
			return;
		}

		for (var i = 0; i < obj.length; i++) {	
					
		    var obje = obj[i];
		   
		                    

			var objJson = new function(){
		        this.id = obje._id;
		        this.userid = obje.userid;
		        this.description = obje.description;
		        this.name = obje.name;
		        this.time = obje.time;
		        this.antecedence = obje.antecedence;
		                      
		    };
		    
			_buff.push(objJson);
												
		};
	            
	    return resultFunction(0,_buff);
	});

}

exports.deleteSchedule = function(req, res) {

  	var resultFunction = function(cod,msg){
		var result = new function(){
			this.v = 1;
			this.cod = cod;
			this.msg = msg;	
		};
        
		// JSON
		res.end(JSON.stringify({
			ret: result
		}));
	};

	var p1_schedule_id = req.query.p1;

	MSchedule.remove({_id : p1_schedule_id}, function(err, obj){

		if(err){
			return resultFunction(2,"erro ao apagar");
		}

		return resultFunction(0,"");

	});
}

