package com.cezimbra.diego.projetodesafioavenuecode.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by diego cezimbra on 09/06/2016.
 */
public class User {

    @SerializedName("provider_id")
    private String providerId;
    @SerializedName("display_name")
    private String displayName;
    @SerializedName("email")
    private String email;
    @SerializedName("photo_url")
    private String photoUrl;
    @SerializedName("uid")
    private String uid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String id) {
        this.providerId = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
