package com.cezimbra.diego.projetodesafioavenuecode.util;

import android.app.Activity;
import android.app.ProgressDialog;


/**
 * Created by diego cezimbra on 11/06/16.
 */
public class UIUtils {

    private static ProgressDialog ringProgressDialog = null;
    public static void showDialog(Activity act, String title, String msg) {

        hideDialog();

        try {
            //Log.e(TCodes.TAG, "launchDialog");
            if (ringProgressDialog != null) {
                ringProgressDialog.dismiss();
                ringProgressDialog = null;
            }

            ringProgressDialog = ProgressDialog.show(act, title, msg, true);
            ringProgressDialog.setCancelable(true);
        }catch (Exception e){

        }catch (Throwable e){

        }
    }

    public static void hideDialog() {
        try {
            //Log.e(TCodes.TAG, "stopDialog");
            if (ringProgressDialog == null) {
                return;
            }
            if(ringProgressDialog.isShowing()) {
                ringProgressDialog.dismiss();
            }
            ringProgressDialog = null;
        }catch (Exception e){
            e.printStackTrace();
        }catch (Throwable e){
            e.printStackTrace();
        }
    }
}
