package com.cezimbra.diego.projetodesafioavenuecode.adapter;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;


import com.cezimbra.diego.projetodesafioavenuecode.R;
import com.cezimbra.diego.projetodesafioavenuecode.interfac.AdapterActions;
import com.cezimbra.diego.projetodesafioavenuecode.model.Schedule;
import com.cezimbra.diego.projetodesafioavenuecode.receiver.AlarmReceiver;
import com.cezimbra.diego.projetodesafioavenuecode.util.TempUtil;
import com.cezimbra.diego.projetodesafioavenuecode.util.Utils;
import com.cezimbra.diego.projetodesafioavenuecode.view.activity.EditScheduleActivity;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by froger_mcs on 05.11.14.
 */

public class ScheduleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_DEFAULT = 1;
    private static final int VIEW_TYPE_LOADER = 2;
    private static final int ANIMATED_ITEMS_COUNT = 2;
    private Activity context;
    private int lastAnimatedPosition = -1;
    private boolean aanimateItems = false;
    private boolean showLoadingView = false;
    private int loadingViewSize = Utils.dpToPx(200);
    private ArrayList<Schedule> schedules;
    private AdapterActions adapterActions;

    public ScheduleAdapter(Activity context, ArrayList<Schedule> schedules, AdapterActions adapterActions) {
        this.context = context;
        this.schedules = schedules;
        this.adapterActions = adapterActions;
        if (schedules == null) {
            schedules = new ArrayList<>();
        }
    }

    public class ListItem {
        public CellRepoViewHolder holder;
        public Schedule schedule;
    }


    private void bindDefaultFeedItem(final int position, CellRepoViewHolder holder) {

        ListItem listItem = new ListItem();
        listItem.holder = holder;

        try {
            final Schedule schedule = schedules.get(position);

            holder.scheduleName.setText(schedule.getName());

            String description = schedule.getDescription();
            String antecedence = schedule.getAntecedence();
            int iAntecedenceinMillis;
            try {
                iAntecedenceinMillis = Integer.parseInt(antecedence) * 60000;
            } catch (Exception e) {
                iAntecedenceinMillis = 0;
            }
            if (description == null || description.equals("null")) {
                description = "";
            }
            if (antecedence == null || antecedence.equals("null")) {
                antecedence = "";
            } else {
                antecedence = antecedence + " minutes before";
            }

            holder.scheduleDescription.setText(description + "\n\n" + antecedence);

            Date date = new Date(Long.parseLong(schedule.getTime()));
            String s = new SimpleDateFormat("HH:mm").format(date);

            Date datewithAntecedence = new Date(Long.parseLong(schedule.getTime()) - iAntecedenceinMillis);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, datewithAntecedence.getHours());
            calendar.set(Calendar.MINUTE, datewithAntecedence.getMinutes());

            if (System.currentTimeMillis() < (Long.parseLong(schedule.getTime()) - iAntecedenceinMillis)) {
                AlarmManager alarmMgr;
                PendingIntent alarmIntent;

                alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                Intent intent = new Intent(context, AlarmReceiver.class);
                alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

                alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                        AlarmManager.RTC_WAKEUP, alarmIntent);
            }

            holder.scheduleTime.setText(s);
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adapterActions.onDelete(position);
                }
            });

            holder.layoutClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Gson gson = new Gson();

                    TempUtil.setSchedule(context, gson.toJson(schedule));
                    context.startActivityForResult(new Intent(context, EditScheduleActivity.class), 1001);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void bindLoadingFeedItem(final CellRepoViewHolder holder) {

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(context).inflate(R.layout.item_schedule, parent, false);
        final CellRepoViewHolder cellFeedViewHolder = new CellRepoViewHolder(view);

        if (viewType == VIEW_TYPE_DEFAULT) {

        } else if (viewType == VIEW_TYPE_LOADER) {
            View bgView = new View(context);
            bgView.setLayoutParams(new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
            ));
            bgView.setBackgroundColor(0x77ffffff);
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(loadingViewSize, loadingViewSize);
            params.gravity = Gravity.CENTER;

        }

        return cellFeedViewHolder;
    }

    private void runEnterAnimation(View view, int position) {
        if (!aanimateItems || position >= ANIMATED_ITEMS_COUNT - 1) {
            return;
        }

        if (position > lastAnimatedPosition) {
            lastAnimatedPosition = position;
            view.setTranslationY(Utils.getScreenHeight(context));
            view.animate()
                    .translationY(0)
                    .setInterpolator(new DecelerateInterpolator(3.f))
                    .setDuration(700)
                    .start();
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        runEnterAnimation(viewHolder.itemView, position);
        final CellRepoViewHolder holder = (CellRepoViewHolder) viewHolder;
        if (getItemViewType(position) == VIEW_TYPE_DEFAULT) {
            bindDefaultFeedItem(position, holder);
        } else if (getItemViewType(position) == VIEW_TYPE_LOADER) {
            bindLoadingFeedItem(holder);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (showLoadingView && position == 0) {
            return VIEW_TYPE_LOADER;
        } else {
            return VIEW_TYPE_DEFAULT;
        }
    }

    @Override
    public int getItemCount() {
        if (schedules != null)
            return schedules.size();

        return 0;
    }

    public void updateItems(boolean animated) {
        aanimateItems = animated;
        notifyDataSetChanged();
    }

    public void showLoadingView() {
        showLoadingView = true;
        notifyItemChanged(0);
    }

    public static class CellRepoViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.scheduleName)
        TextView scheduleName;

        @InjectView(R.id.scheduleDescription)
        TextView scheduleDescription;

        @InjectView(R.id.scheduleTime)
        TextView scheduleTime;

        @InjectView(R.id.delete)
        View delete;

        @InjectView(R.id.ll1)
        View layoutClick;

        public CellRepoViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }

}
