package com.cezimbra.diego.projetodesafioavenuecode.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.cezimbra.diego.projetodesafioavenuecode.R;
import com.cezimbra.diego.projetodesafioavenuecode.adapter.ScheduleAdapter;
import com.cezimbra.diego.projetodesafioavenuecode.database.ScheduleBO;
import com.cezimbra.diego.projetodesafioavenuecode.interfac.AdapterActions;
import com.cezimbra.diego.projetodesafioavenuecode.interfac.WebServiceCallback;
import com.cezimbra.diego.projetodesafioavenuecode.listener.EndlessRepositorioListener;
import com.cezimbra.diego.projetodesafioavenuecode.model.Schedule;
import com.cezimbra.diego.projetodesafioavenuecode.util.TempUtil;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import butterknife.InjectView;


public class MainActivity extends BaseActivity implements WebServiceCallback {

    @InjectView(R.id.rvList)
    RecyclerView rvList;

    private ScheduleAdapter adapter;
    private ArrayList<Schedule> schedules;
    private int page = 1;
    private EndlessRepositorioListener listener = null;
    private LinearLayoutManager linearLayoutManager;
    private String LIST_STATE_KEY = "layout_manager";
    private String LIST = "array_list";
    private Parcelable mListState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayoutManager = new LinearLayoutManager(this) {
            @Override
            protected int getExtraLayoutSpace(RecyclerView.State state) {
                return 300;
            }
        };
        rvList.setLayoutManager(linearLayoutManager);

        if (savedInstanceState != null) {
            schedules = (ArrayList<Schedule>) savedInstanceState.getSerializable(LIST);
        } else {
            schedules = new ArrayList<>();
        }

        if (schedules == null)
            schedules = new ArrayList<>();

        adapter = new ScheduleAdapter(this, schedules, adapterActions);
        rvList.setAdapter(adapter);

        listener = new EndlessRepositorioListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                ScheduleBO.getSchedules(MainActivity.this, current_page, MainActivity.this);
            }
        };

        rvList.addOnScrollListener(listener);

        adapter.updateItems(true);

        if (schedules.size() == 0)
            ScheduleBO.getSchedules(this, page, this);

        loadList(TempUtil.getSchedules(this));
    }

    AdapterActions adapterActions = new AdapterActions() {
        @Override
        public void onDelete(int position) {
           Schedule removed = schedules.get(position);
            schedules.remove(position);
            adapter.notifyDataSetChanged();
            ScheduleBO.removeSchedule(MainActivity.this, removed, new WebServiceCallback() {
                @Override
                public void onResult(String result) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(result);

                        obj = obj.getJSONObject("ret");
                        int cod = obj.getInt("cod");

                        if (cod != 0) {
                            showMsg("Error " + obj.getString("msg"));
                        }

                        ScheduleBO.getSchedules(MainActivity.this, page, MainActivity.this);

                    } catch (JSONException e) {
                        showMsg("Error geting data");
                    }
                }

                @Override
                public void onError(String error) {

                }

                @Override
                public void onServiceUnavaliableOrNotFound(String error) {

                }

                @Override
                public void onNoInternet(String error) {

                }

                @Override
                public void onProgress(int size, int total) {

                }
            });
        }
    };

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        mListState = linearLayoutManager.onSaveInstanceState();
        outState.putParcelable(LIST_STATE_KEY, mListState);
        outState.putSerializable(LIST, schedules);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mListState = linearLayoutManager.onSaveInstanceState();
        outState.putParcelable(LIST_STATE_KEY, mListState);
        outState.putSerializable(LIST, schedules);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mListState != null) {
            linearLayoutManager.onRestoreInstanceState(mListState);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            mListState = savedInstanceState.getParcelable(LIST_STATE_KEY);

            ArrayList<Schedule> temp = (ArrayList<Schedule>) savedInstanceState.getSerializable(LIST);
            if (temp != null)
                schedules = temp;
        }
    }

    @Override
    public void onResult(String result) {

        String oldResult = TempUtil.getSchedules(this);

        if (oldResult != null && oldResult.equals(result)) {
            return;
        }

        loadList(result);
    }

    private void loadList(String result) {
        if (result == null) {
            return;
        }

        Gson gson = new Gson();
        JSONObject obj = null;
        try {
            obj = new JSONObject(result);

            obj = obj.getJSONObject("ret");
            int cod = obj.getInt("cod");

            if (cod != 0) {

                if(cod == -2 || cod == -3){
                    TempUtil.setSchedules(MainActivity.this, "");
                }

                showMsg(obj.getString("msg"));
                return;
            }

            JSONArray arr = obj.getJSONArray("msg");
            schedules.clear();
            for (int i = 0; i < arr.length(); i++) {
                Schedule rep = gson.fromJson(arr.getJSONObject(i).toString(), Schedule.class);
                schedules.add(rep);
            }

            TempUtil.setSchedules(this, result);
            adapter.notifyDataSetChanged();

        } catch (JSONException e) {
            showMsg("Error geting data");
        }
    }

    @Override
    public void onError(String error) {
        Log.e("Github", "MainActviity onError error = " + error);
        showMsg("MainActviity onError error = " + error);
    }

    @Override
    public void onServiceUnavaliableOrNotFound(String error) {
        Log.e("Github", "MainActviity onServiceUnavaliableOrNotFound error = " + error);
        showMsg("MainActviity onServiceUnavaliableOrNotFound error = " + error);
    }

    @Override
    public void onNoInternet(String error) {
        Log.e("Github", "MainActviity onNoInternet error = " + error);
        showMsg("MainActviity onNoInternet error = " + error);
    }

    @Override
    public void onProgress(int size, int total) {
        Log.e("Github", "MainActviity onProgress size = " + size + " total = " + total);
        showMsg("MainActviity onProgress size = " + size + " total = " + total);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {
            ScheduleBO.getSchedules(this, page, this);
        }else if(requestCode == 1001){
            ScheduleBO.getSchedules(this, page, this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new:
                startActivityForResult(new Intent(this, AddScheduleActivity.class), 100);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
