package com.cezimbra.diego.projetodesafioavenuecode.view.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TimePicker;


import com.cezimbra.diego.projetodesafioavenuecode.interfac.TimePickerEnd;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by cezimbra on 11/06/2016.
 */
public class TimePickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    private static TimePickerEnd timePickerEnd;
    public static TimePickerFragment newInstance(TimePickerEnd _timePickerEnd){
        timePickerEnd = _timePickerEnd;
        return new TimePickerFragment();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        timePickerEnd.onTimePicked(hourOfDay, minute);
    }
}
