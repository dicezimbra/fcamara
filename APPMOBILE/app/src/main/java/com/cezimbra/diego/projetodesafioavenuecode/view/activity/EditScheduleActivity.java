package com.cezimbra.diego.projetodesafioavenuecode.view.activity;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cezimbra.diego.projetodesafioavenuecode.R;
import com.cezimbra.diego.projetodesafioavenuecode.database.ScheduleBO;
import com.cezimbra.diego.projetodesafioavenuecode.database.UserBO;
import com.cezimbra.diego.projetodesafioavenuecode.interfac.TimePickerEnd;
import com.cezimbra.diego.projetodesafioavenuecode.interfac.WebServiceCallback;
import com.cezimbra.diego.projetodesafioavenuecode.model.Schedule;
import com.cezimbra.diego.projetodesafioavenuecode.util.TempUtil;
import com.cezimbra.diego.projetodesafioavenuecode.util.UIUtils;
import com.cezimbra.diego.projetodesafioavenuecode.view.fragment.TimePickerFragment;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

import butterknife.InjectView;

public class EditScheduleActivity extends BaseActivity implements TimePickerEnd, WebServiceCallback {
    private int hours;
    private int minutes;

    @InjectView(R.id.switch_compat)
    SwitchCompat activateAlarm;

    @InjectView(R.id.timeBefore)
    EditText timeBefore;

    @InjectView(R.id.reminderhour)
    TextView reminderhour;

    @InjectView(R.id.edit)
    Button editButton;

    @InjectView(R.id.name)
    EditText scheduleName;

    @InjectView(R.id.description)
    EditText scheduleDescription;
    private Schedule tempSchedule;
    private DialogFragment newFragment;
    private FragmentManager manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_schedule);

        Gson gson = new Gson();
        tempSchedule = gson.fromJson(TempUtil.getSchedule(this), Schedule.class);

        if(tempSchedule != null){
            Calendar rightNow = Calendar.getInstance();
            rightNow.setTimeInMillis(Long.parseLong(tempSchedule.getTime()));
            reminderhour.setText(
                    rightNow.get(Calendar.HOUR) + ":" +
                            rightNow.get(Calendar.MINUTE));

            scheduleName.setText(tempSchedule.getName());
            scheduleDescription.setText(tempSchedule.getDescription());
            if(tempSchedule.getAntecedence() != null && !tempSchedule.getAntecedence().equals("")){
                activateAlarm.setChecked(true);
                timeBefore.setVisibility(View.VISIBLE);
                timeBefore.setText(tempSchedule.getAntecedence());
            }
        }


        newFragment = TimePickerFragment.newInstance(this);

        manager = getFragmentManager();

        activateAlarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    timeBefore.setVisibility(View.VISIBLE);
                } else {
                    timeBefore.setVisibility(View.GONE);
                }
            }
        });

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newFragment.show(manager, "timePicker");
            }
        });
    }

    @Override
    public void onTimePicked(int hour, int minute) {
        reminderhour.setText(hour + ":" + minute);
        this.hours = hour;
        this.minutes = minute;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_schedule, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done:

                Schedule schedule = new Schedule();

                schedule.setAntecedence(timeBefore.getText().toString());
                schedule.setDescription(scheduleDescription.getText().toString());
                schedule.setName(scheduleName.getText().toString());
                schedule.setUserid(UserBO.getUser(EditScheduleActivity.this).getUid());
                schedule.setId(tempSchedule.getId());
                Date date = new Date();
                date.setHours(this.hours);
                date.setMinutes(this.minutes);

                long time = date.getTime();
                schedule.setTime("" + time);

                if (TextUtils.isEmpty(schedule.getName())) {
                    showMsg("Name field cannot be empty");
                    return true;
                }

                if (TextUtils.isEmpty(schedule.getTime())) {
                    showMsg("Select the time");
                    return true;
                }

                if (TextUtils.isEmpty(schedule.getUserid())) {
                    return true;
                }

                if (TextUtils.isEmpty(schedule.getAntecedence())) {
                    schedule.setAntecedence("");
                }

                if (TextUtils.isEmpty(schedule.getDescription())) {
                    schedule.setDescription("");
                }

                UIUtils.showDialog(this, "Wait", "We are saving your schedule");
                ScheduleBO.editSchedule(this, schedule, this);

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResult(String result) {
        UIUtils.hideDialog();
        JSONObject obj = null;
        try {
            obj = new JSONObject(result);

            obj = obj.getJSONObject("ret");
            int cod = obj.getInt("cod");

            if (cod != 0) {
                Toast.makeText(EditScheduleActivity.this, obj.getString("msg"), Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(EditScheduleActivity.this, "Edited with success", Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        this.finish();
    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void onServiceUnavaliableOrNotFound(String error) {

    }

    @Override
    public void onNoInternet(String error) {

    }

    @Override
    public void onProgress(int size, int total) {

    }
}
