package com.cezimbra.diego.projetodesafioavenuecode.database;

import android.app.Activity;
import android.util.Log;

import com.cezimbra.diego.projetodesafioavenuecode.interfac.WebServiceCallback;
import com.cezimbra.diego.projetodesafioavenuecode.model.Schedule;
import com.cezimbra.diego.projetodesafioavenuecode.util.WebServicesUtils;
import com.cezimbra.diego.projetodesafioavenuecode.view.activity.EditScheduleActivity;
import com.cezimbra.diego.projetodesafioavenuecode.view.activity.MainActivity;
import com.google.gson.Gson;

import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import java.util.Vector;

/**
 * Created by diego cezimbra on 09/06/2016.
 */
public class ScheduleBO extends AppBO{

    public static int addSchedule(Activity act, Schedule schedule, WebServiceCallback c){
        String mBaseUrl = WebServicesUtils.getBaseUrlV1();
        String mUrl = mBaseUrl + "/addschedule";

        Gson gson = new Gson();
        StringEntity entity = WebServicesUtils.postEntity(gson.toJson(schedule));

        WebServicesUtils.post(act, entity, mUrl, c);
        return 0;
    }


    public static int getSchedules(Activity act, int current_page, WebServiceCallback c) {
        Vector<String> params = new Vector<>();

        try {
            String mBaseUrl = WebServicesUtils.getBaseUrlV1() + "/getschedules";
            params.add(UserBO.getUser(act).getUid());
            String mUrl = WebServicesUtils.getUrlQuery(mBaseUrl, params);
            Log.e("AvenueCode", "ScheduleBO getSchedules mUrl = "+mUrl);
            WebServicesUtils.get(act, mUrl, c);
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    public static int removeSchedule(MainActivity act, Schedule removed, WebServiceCallback c) {

        Vector<String> params = new Vector<>();

        try {
            String mBaseUrl = WebServicesUtils.getBaseUrlV1() + "/deleteschedule";
            params.add(removed.getId());
            String mUrl = WebServicesUtils.getUrlQuery(mBaseUrl, params);
            Log.e("AvenueCode", "ScheduleBO deleteschedule mUrl = "+mUrl);
            WebServicesUtils.get(act, mUrl, c);
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    public static int editSchedule(Activity act, Schedule schedule, WebServiceCallback c) {
        String mBaseUrl = WebServicesUtils.getBaseUrlV1();
        String mUrl = mBaseUrl + "/editschedule";

        Gson gson = new Gson();
        StringEntity entity = WebServicesUtils.postEntity(gson.toJson(schedule));

        WebServicesUtils.post(act, entity, mUrl, c);
        return 0;
    }
}
