package com.cezimbra.diego.projetodesafioavenuecode.database;

import android.content.Context;

import com.cezimbra.diego.projetodesafioavenuecode.model.User;
import com.google.gson.Gson;

/**
 * Created by diego cezimbra on 09/06/2016.
 */
public class UserBO extends AppBO {

    public static void setUser(Context ctx, User user){
        Gson gson = new Gson();
        save(ctx, "user", gson.toJson(user));
    }

    public static User getUser(Context ctx){
        Gson gson = new Gson();
        String user = load(ctx, "user");
        return gson.fromJson(user, User.class);
    }
}
