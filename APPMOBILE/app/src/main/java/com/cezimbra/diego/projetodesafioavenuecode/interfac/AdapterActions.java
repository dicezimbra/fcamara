package com.cezimbra.diego.projetodesafioavenuecode.interfac;

/**
 * Created by cezimbra on 12/06/2016.
 */
public interface AdapterActions {

    public void onDelete(int position);
}
