package com.cezimbra.diego.projetodesafioavenuecode.database;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by diego on 09/06/2016.
 */
public class AppBO {

    protected static String load(Context ctx, String key) {
        try {
            SharedPreferences settings = ctx.getSharedPreferences(
                    "bd", Context.MODE_PRIVATE);
            return settings.getString(key, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    protected static int save(Context ctx, String key, String data) {
        try {
            SharedPreferences settings = ctx.getSharedPreferences(
                    "bd", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(key, data);
            editor.commit();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

}
