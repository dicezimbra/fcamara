package com.cezimbra.diego.projetodesafioavenuecode.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by diego cezimbra on 09/06/2016.
 */
public class Schedule implements Serializable {

    @SerializedName("id")
    private String id;

    @SerializedName("userid")
    private String userid;

    @SerializedName("description")
    private String description;

    @SerializedName("name")
    private String name;

    @SerializedName("time")
    private String time;

    @SerializedName("antecedence")
    private String antecedence;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAntecedence() {
        return antecedence;
    }

    public void setAntecedence(String antecedence) {
        this.antecedence = antecedence;
    }
}
