package com.cezimbra.diego.projetodesafioavenuecode.util;

import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by cezimbra on 11/06/2016.
 */
public class TempUtil {

    public static String getSchedules(Activity act) {
        SharedPreferences shared = act.getSharedPreferences("bd", act.MODE_PRIVATE);
        if (shared == null) {
            return null;
        }
        String json = shared.getString("tempData", null);
        return json;

    }

    public static int setSchedules(Activity act, String schedules) {
        SharedPreferences shared = act.getSharedPreferences("bd", act.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString("tempData", schedules);
        editor.commit();
        return 0;
    }


    public static String getSchedule(Activity act) {
        SharedPreferences shared = act.getSharedPreferences("bd", act.MODE_PRIVATE);
        if (shared == null) {
            return null;
        }
        String json = shared.getString("tempSchedule", null);
        return json;

    }

    public static int setSchedule(Activity act, String schedule) {
        SharedPreferences shared = act.getSharedPreferences("bd", act.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString("tempSchedule", schedule);
        editor.commit();
        return 0;
    }
}
