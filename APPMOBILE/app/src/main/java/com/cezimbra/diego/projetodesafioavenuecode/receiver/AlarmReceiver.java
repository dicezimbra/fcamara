package com.cezimbra.diego.projetodesafioavenuecode.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.widget.Toast;

import com.cezimbra.diego.projetodesafioavenuecode.util.NotificationUtil;

/**
 * Created by cezimbra on 12/06/2016.
 */
public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        // Vibrate the mobile phone
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = { 0, 100, 500, 100, 500, 100, 500, 100, 500, 100, 500};
        vibrator.vibrate(pattern , -1);

        NotificationUtil.showNotification(context, "Schedule warn","Don't panik but your time is up!!!!.");
    }
}
