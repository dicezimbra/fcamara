package com.cezimbra.diego.projetodesafioavenuecode.view.activity;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.text.TextUtilsCompat;
import android.support.v7.widget.SwitchCompat;
import android.test.UiThreadTest;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cezimbra.diego.projetodesafioavenuecode.R;
import com.cezimbra.diego.projetodesafioavenuecode.database.ScheduleBO;
import com.cezimbra.diego.projetodesafioavenuecode.database.UserBO;
import com.cezimbra.diego.projetodesafioavenuecode.interfac.TimePickerEnd;
import com.cezimbra.diego.projetodesafioavenuecode.interfac.WebServiceCallback;
import com.cezimbra.diego.projetodesafioavenuecode.model.Schedule;
import com.cezimbra.diego.projetodesafioavenuecode.util.*;
import com.cezimbra.diego.projetodesafioavenuecode.view.fragment.TimePickerFragment;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.InjectView;

public class AddScheduleActivity extends BaseActivity implements TimePickerEnd, WebServiceCallback {
    private int hours;
    private int minutes;

    @InjectView(R.id.switch_compat)
    SwitchCompat activateAlarm;

    @InjectView(R.id.timeBefore)
    EditText timeBefore;

    @InjectView(R.id.reminderhour)
    TextView reminderhour;

    @InjectView(R.id.edit)
    Button editButton;

    @InjectView(R.id.name)
    EditText scheduleName;

    @InjectView(R.id.description)
    EditText scheduleDescription;

    private DialogFragment newFragment;
    private FragmentManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_schedule);
        newFragment = TimePickerFragment.newInstance(this);

        manager = getFragmentManager();
        newFragment.show(manager, "timePicker");

        activateAlarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    timeBefore.setVisibility(View.VISIBLE);
                } else {
                    timeBefore.setVisibility(View.GONE);
                }
            }
        });

        Calendar rightNow = Calendar.getInstance();
        reminderhour.setText(
                rightNow.get(Calendar.HOUR) + ":" +
                        rightNow.get(Calendar.MINUTE));

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newFragment.show(manager, "timePicker");
            }
        });
    }

    @Override
    public void onTimePicked(int hour, int minute) {
        reminderhour.setText(hour + ":" + minute);
        this.hours = hour;
        this.minutes = minute;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_schedule, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done:

                Schedule schedule = new Schedule();

                schedule.setAntecedence(timeBefore.getText().toString());
                schedule.setDescription(scheduleDescription.getText().toString());
                schedule.setName(scheduleName.getText().toString());
                schedule.setUserid(UserBO.getUser(AddScheduleActivity.this).getUid());
                Date date = new Date();
                date.setHours(this.hours);
                date.setMinutes(this.minutes);

                long time = date.getTime();
                schedule.setTime("" + time);

                if (TextUtils.isEmpty(schedule.getName())) {
                    showMsg("Name field cannot be empty");
                    return true;
                }

                if (TextUtils.isEmpty(schedule.getTime())) {
                    showMsg("Select the time");
                    return true;
                }

                if (TextUtils.isEmpty(schedule.getUserid())) {
                    return true;
                }

                if (TextUtils.isEmpty(schedule.getAntecedence())) {
                    schedule.setAntecedence("");
                }

                if (TextUtils.isEmpty(schedule.getDescription())) {
                    schedule.setDescription("");
                }

                UIUtils.showDialog(this, "Wait", "We are saving your schedule");
                ScheduleBO.addSchedule(this, schedule, this);

        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onResult(String result) {
        UIUtils.hideDialog();
        JSONObject obj = null;
        try {
            obj = new JSONObject(result);

            obj = obj.getJSONObject("ret");
            int cod = obj.getInt("cod");

            if (cod != 0) {
                Toast.makeText(AddScheduleActivity.this, obj.getString("msg"), Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(AddScheduleActivity.this, "Added with success", Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        this.finish();
    }

    @Override
    public void onError(String error) {
        Toast.makeText(this, "Error " + error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServiceUnavaliableOrNotFound(String error) {
        Toast.makeText(this, "Error " + error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNoInternet(String error) {
        Toast.makeText(this, "Error " + error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProgress(int size, int total) {

    }
}
