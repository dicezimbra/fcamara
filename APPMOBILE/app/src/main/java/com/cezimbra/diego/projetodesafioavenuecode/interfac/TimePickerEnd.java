package com.cezimbra.diego.projetodesafioavenuecode.interfac;

/**
 * Created by cezimbra on 11/06/2016.
 */
public interface TimePickerEnd {
    public void onTimePicked(int hour, int minute);
}
